module Main where

import qualified Data.Map                      as Map

import           Graphics.OpenSCAD
import           Data.Colour

twenty = 20
fnsomething = 10

facets = fn fnsomething

keylength = 19
halfkeylength = keylength / 2
quarterkeylength = halfkeylength / 2
eigthkeylength = quarterkeylength / 2

switchlength = 13.97

axislength = 120
axishole = axislength + 10

materialthickness = 1.5
screwdiameter = 1.5
screwradius = screwdiameter / 2

pentagon :: Double -> Double -> Double -> Model2d
pentagon width height depth =
  let diff = width - depth
      nul  = 0
  in  polygon
        0
        [ [ (nul  , nul)
          , (diff , nul)
          , (width, depth)
          , (width, height)
          , (nul  , height)
          ]
        ]

hexagon width height depth = union
  [pentagon width height depth, mirror (1, 0) (pentagon width height depth)]

bendingarea width height depth =
  let x = width / 2
      y = height / 2
  in  union [hexagon x y depth, mirror (0, 1) (hexagon x y depth)]

singlekey = square keylength True

singleswitch = square switchlength True

singleholedkey = difference singlekey singleswitch

singleandhalfholedkey =
  difference (rectangle keylength (keylength * 1.5) True) singleswitch

twoandhalfholedkey = union
  [ singleandhalfholedkey
  , translate (0, (keylength * 1.5 / 2) + halfkeylength) singleholedkey
  ]

twoonequarterkey =
  let onequarter = difference (rectangle keylength (keylength * 1.25) True)
                              (square switchlength True)
  in  union [onequarter, translate (0, keylength * 1.25) onequarter]

switchcolumn key = union $ map
  (\i -> translate ((0, i * keylength) :: Vector2d) singleholedkey)
  [0 .. key - 1]

framecolumn key = union $ map
  (\i -> translate ((0, i * keylength) :: Vector2d) singlekey)
  [0 .. key - 1]

thumbcolumn key
  | key < 1 = error "should be more than 1"
  | key == 1 = twoandhalfholedkey
  | otherwise = union $ (twoandhalfholedkey) : map
    (\i -> translate ((i * keylength, -eigthkeylength) :: Vector2d)
                     twoonequarterkey
    )
    [1 .. key - 1]

screwarea diameter =
  foldl difference (circle halfkeylength facets)
    $ [ translate (0, halfkeylength)     singlekey
      , translate (0, -quarterkeylength) (circle (diameter / 2) facets)
      ]

thumbscrewarea key = foldl difference bigger smallers
 where
  bigger = hull
    [ translate (keylength * key / 4, 0) $ circle halfkeylength facets
    , translate (-keylength * key / 4, 0) $ circle halfkeylength facets
    ]
  smallers = [halfrect, screwline]
  halfrect =
    translate (0, -halfkeylength) $ rectangle (keylength * key) keylength True
  screwline = hull
    [ translate (-(keylength * key * 0.3), screwradius)
      $ circle screwradius facets
    , translate (keylength * key * 0.3, screwradius) $ circle screwradius facets
    ]

switchcolumnplate key = union
  [ translate (0, key * keylength) $ mirror (0, 1) $ screwarea screwdiameter
  , translate (0, key * keylength - quarterkeylength)
    $ bendingarea keylength halfkeylength 2.5
  , switchcolumn key
  , translate (0, -(halfkeylength * 1.5))
    $ bendingarea keylength halfkeylength 2.5
  , translate (0, -keylength) $ screwarea screwdiameter
  ]

screwareav2 r screwr =
  let diam = r * 2
  in  foldl difference (circle r facets)
        $ [ translate (0, -r) $ square diam True
          , translate (0, r / 2) (circle screwr facets)
          ]

thumbscrewareav2 =
  let
    screw = hull
      [ translate (halfkeylength, 0) $ circle halfkeylength facets
      , translate (-halfkeylength, 0) $ circle halfkeylength facets
      ]
    halfhelper =
      translate (0, -halfkeylength) $ rectangle (keylength * 2) keylength True
    screwline = hull
      [ translate (halfkeylength, 5) $ circle screwradius facets
      , translate (-halfkeylength, 5) $ circle screwradius facets
      ]
  in
    foldl difference screw [halfhelper, screwline]

switchcolumnplatev2 keycount bendinglength =
  let
    columnoffset  = (keycount - 1) * keylength / 2
    bendingoffset = keycount * keylength / 2 + bendinglength / 2
    screwoffset   = keycount * keylength / 2 + bendinglength
    bending =
      translate (0, bendingoffset) $ bendingarea keylength bendinglength 2.5
    screw = translate (0, screwoffset) $ screwareav2 halfkeylength screwradius
  in
    union
      [ translate (0, -columnoffset) $ switchcolumn keycount
      , bending
      , mirror (0, 1) bending
      , screw
      , mirror (0, 1) screw
      ]

switchcolumnframev2 keycount bendinglength =
  let
    bendingoffset =
      keycount * keylength / 2 + materialthickness + bendinglength / 2
    walloffset =
      keycount
        * keylength
        / 2
        + materialthickness
        + bendinglength
        + halfkeylength
    screwoffset =
      keycount * keylength / 2 + materialthickness + bendinglength + keylength
    framebody =
      rectangle keylength (keylength * keycount + materialthickness * 2) True
    bending =
      translate (0, bendingoffset) $ bendingarea keylength bendinglength 2.5
    wall  = translate (0, walloffset) singlekey
    screw = translate (0, screwoffset) $ screwareav2 halfkeylength screwradius
    frame = union
      [ framebody
      , bending
      , mirror (0, 1) bending
      , wall
      , mirror (0, 1) wall
      , screw
      , mirror (0, 1) screw
      ]
    screwline = rectangle
      screwdiameter
      ( keycount
      * keylength
      + (bendinglength + keylength + materialthickness)
      * 2
      + halfkeylength
      )
      True
  in
    difference frame screwline

oneandhalfkeyv2 =
  let plate = scale (1, 1.5) singlekey in difference plate singleswitch

twoandhalfholedkeyv2 =
  union [oneandhalfkeyv2, translate (0, 1.25 * keylength) singleholedkey]

thumbcolumnv2 =
  union [twoandhalfholedkeyv2, translate (keylength, 0) twoandhalfholedkeyv2]

thumbclusterv2 bendinglength =
  let
    plate         = translate (-halfkeylength, -0.5 * keylength) thumbcolumnv2
    bendingoffset = keylength * 2.5 / 2 + bendinglength / 2
    screwoffset   = keylength * 2.5 / 2 + bendinglength
    bending =
      translate (0, bendingoffset) $ bendingarea (keylength * 2) bendinglength 5
    screw = translate (0, screwoffset) $ thumbscrewareav2
  in
    union [plate, bending, mirror (0, 1) bending, screw, mirror (0, 1) screw]

switchcolumnframe key = difference frame screwline
 where
  halfthick = materialthickness / 2
  screwline = translate (0, (key / 2 - 0.5) * keylength)
    $ rectangle screwdiameter ((key + 3.5) * keylength) True
  frame = union
    [ translate (0, key * keylength + keylength) $ mirror (0, 1) $ screwarea
      screwdiameter
    , translate (0, key * keylength + halfkeylength) singlekey
    , translate (0, key * keylength - quarterkeylength + materialthickness)
      $ bendingarea keylength halfkeylength 2.5
    , translate (0, key * keylength - halfkeylength + halfthick)
      $ rectangle keylength materialthickness True
    , framecolumn key
    , translate (0, -(halfkeylength + halfthick))
      $ rectangle keylength materialthickness True
    , translate (0, -(halfkeylength + quarterkeylength + halfthick))
      $ bendingarea keylength halfkeylength 2.5
    , translate (0, -(keylength * 1.5)) singlekey
    , translate (0, -(keylength * 2)) $ screwarea screwdiameter
    ]

thumbcluster key = union
  [ translate
      (halfkeylength, keylength * 1.25 / 2 + keylength * 2 + eigthkeylength)
    $ thumbscrewarea key
  , translate
      (halfkeylength, keylength * 1.25 / 2 + keylength * 3 / 2 + eigthkeylength)
    $ bendingarea (keylength * key) keylength quarterkeylength
  , thumbcolumn key
  , translate (halfkeylength, -keylength - eigthkeylength)
    $ bendingarea (keylength * key) keylength quarterkeylength
  , translate (halfkeylength, -keylength * 1.5 - eigthkeylength)
  $ rotate (180, 0)
  $ thumbscrewarea key
  ]

thumbframe = foldl difference bigger smallers
 where
  fifthkeylength      = keylength / 5
  thumbaxislength     = keylength * 7.5
  twoandhalfkeylength = keylength * 2.5
  bigger              = rectangle keylength thumbaxislength True
  smallers            = [firstline, secondline, firstbending, secondbending]
  firstline           = hull
    [ translate (fifthkeylength, thumbaxislength / 2 - 5)
      $ circle screwradius facets
    , translate (fifthkeylength, -thumbaxislength / 2 + 5)
      $ circle screwradius facets
    ]
  secondline = hull
    [ translate (-fifthkeylength, thumbaxislength / 2 - 5)
      $ circle screwradius facets
    , translate (-fifthkeylength, -thumbaxislength / 2 + 5)
      $ circle screwradius facets
    ]
  firstbending      = translate (0, keylength * 1.6) bendingareasimple
  secondbending     = translate (0, -keylength * 1.6) bendingareasimple
  bendingareasimple = union
    [ translate (halfkeylength - 2.5, 0)
      $ polygon 5 [[(0, 0), (2.5, 2.5), (2.5, -2.5)]]
    , translate (-halfkeylength + 2.5, 0)
      $ polygon 5 [[(0, 0), (-2.5, 2.5), (-2.5, -2.5)]]
    ]

mainframe length linelength = difference frame screwline
 where
  halflength = length / 2
  halfline   = linelength / 2
  screwline  = hull
    [ translate (halfline, 0) $ circle screwradius facets
    , translate (-halfline, 0) $ circle screwradius facets
    ]
  frame = hull
    [ translate (halflength, 0) $ circle halfkeylength facets
    , translate (-halflength, 0) $ circle halfkeylength facets
    ]

thumbbridge = difference frame screwline
 where
  halflength = keylength * 2
  halfline   = keylength * 2 + 5
  screwline  = hull
    [ translate (0, halfline) $ circle screwradius facets
    , translate (0, -halfline) $ circle screwradius facets
    ]
  frame = hull
    [ translate (0, halflength) $ circle halfkeylength facets
    , translate (0, -halflength) $ circle halfkeylength facets
    ]

main =
  writeFile "retarded-snek.scad"
    $ renderL
    $ [ switchcolumnplatev2 3 halfkeylength
      , translate (twenty * 1, 0) $ switchcolumnplatev2 4 halfkeylength
      , translate (twenty * 5, 0) $ thumbclusterv2 10
      ]

notmain :: IO ()
notmain = do
  writeFile "retarded-snek.scad"
    $ renderL
    $ map
        (translate (10, 110))
        -- column plates
        -- 3 x 6 and 4 x 4
        [ translate (twenty * 0, 0) $ switchcolumnplate 3
        , translate (twenty * 1, 0) $ switchcolumnplate 3
        , translate (twenty * 2, 0) $ switchcolumnplate 3
        , translate (twenty * 3, 0) $ switchcolumnplate 3
        , translate (twenty * 4, 0) $ switchcolumnplate 3
        , translate (twenty * 5, 0) $ switchcolumnplate 3
        , translate (twenty * 6, 0) $ switchcolumnplate 4
        , translate (twenty * 7, 0) $ switchcolumnplate 4
        , translate (twenty * 8, 0) $ switchcolumnplate 4
        , translate (twenty * 9, 0) $ switchcolumnplate 4
        -- column plates
        -- 4 x 4 and 3 x 6
        , translate (twenty * 0, 6 * twenty) $ switchcolumnframe 4
        , translate (twenty * 1, 6 * twenty) $ switchcolumnframe 4
        , translate (twenty * 2, 6 * twenty) $ switchcolumnframe 4
        , translate (twenty * 3, 6 * twenty) $ switchcolumnframe 4
        , translate (twenty * 4, 7 * twenty) $ switchcolumnframe 3
        , translate (twenty * 5, 7 * twenty) $ switchcolumnframe 3
        , translate (twenty * 6, 7 * twenty) $ switchcolumnframe 3
        , translate (twenty * 7, 7 * twenty) $ switchcolumnframe 3
        , translate (twenty * 8, 7 * twenty) $ switchcolumnframe 3
        , translate (twenty * 9, 7 * twenty) $ switchcolumnframe 3
        -- main frames
        -- 6 x 8.
        , translate (twenty * 3, -twenty * 2)
          $ mainframe (keylength * 6) (keylength * 6 + quarterkeylength)
        , translate (twenty * 3, -twenty * 3)
          $ mainframe (keylength * 6) (keylength * 6 + quarterkeylength)
        , translate (twenty * 3, -twenty * 4)
          $ mainframe (keylength * 6) (keylength * 6 + quarterkeylength)
        , translate (twenty * 3, -twenty * 5)
          $ mainframe (keylength * 6) (keylength * 6 + quarterkeylength)
        , translate (twenty * 10, -twenty * 2)
          $ mainframe (keylength * 6) (keylength * 6 + quarterkeylength)
        , translate (twenty * 10, -twenty * 3)
          $ mainframe (keylength * 6) (keylength * 6 + quarterkeylength)
        , translate (twenty * 10, -twenty * 4)
          $ mainframe (keylength * 6) (keylength * 6 + quarterkeylength)
        , translate (twenty * 10, -twenty * 5)
          $ mainframe (keylength * 6) (keylength * 6 + quarterkeylength)
        -- thumb clusters
        -- 1.5 + 1 + 1.25 x 2 twice.
        , translate (twenty * 10, 15) $ thumbcluster 2
        , translate (twenty * 12, 15) $ thumbcluster 2
        -- thumb frames
        , translate (twenty * 10, 8 * twenty) thumbframe
        , translate (twenty * 11, 8 * twenty) thumbframe
        -- thumb bridges
        , translate (twenty * 12, 7 * twenty) $ thumbbridge
        , translate (twenty * 13, 7 * twenty) $ thumbbridge
        ]
